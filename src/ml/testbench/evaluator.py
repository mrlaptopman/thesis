import tensorflow as tf
import sys
from pathlib import Path


def get_confidence(score, num_labels):
    return num_labels * (score - 1.0 / num_labels)


class Evaluator:
    def __init__(self, root, model, labels):
        self.root = Path(root)
        self.model = Path(model)
        self.labels = Path(labels)
        self.correct = []
        self.incorrect = []
        self.best = []
        self.worst = []
        self.status = {}
        self.score = {}
        self.confidence = {}
        self.avg_confidence = 0.0

        # loads label file, strip carriage return
        label_lines = [line.rstrip() for line in tf.gfile.GFile(self.labels.as_posix())]

        # unpersist graph from file
        with tf.gfile.FastGFile(self.model.as_posix(), 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            _ = tf.import_graph_def(graph_def, name='')

        # create tensorflow session. Use session to give image to the model
        with tf.Session() as sess:
            # softmax function is final layer in our model, it gives us a probability
            softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')

            # read in labels
            # todo: assert that these labels match those in the label file
            correct_labels = [Path(self.root, f) for f in self.root.iterdir() if
                              Path(self.root.as_posix(), f).is_dir()]

            for l in correct_labels:
                self.status[l] = {}
                self.score[l] = {}
                self.confidence[l] = {}

                # loop through each label directory and evaluate the model on its contents
                i = 1.0
                # for file_name in [join(l, f) for f in listdir(l) if isfile(join(l, f))]:
                curr_best = 0
                curr_worst = 1
                for file_name in [Path(l, f) for f in Path(l).iterdir() if Path(l, f).is_file()]:
                    image_data = tf.gfile.FastGFile(file_name.as_posix(), 'rb').read()

                    # get predictions
                    predictions = sess.run(softmax_tensor, {'DecodeJpeg/contents:0': image_data})

                    # sort to show labels in order of confidence
                    top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]

                    classification = label_lines[top_k[0]]
                    score = predictions[0][top_k[0]]
                    confidence = get_confidence(score, len(correct_labels))

                    if classification == l.name:
                        status = "CORRECT"
                        self.correct.append(file_name)
                        self.best = top_n(self.best, file_name, confidence, 1, True)
                    else:
                        status = "INCORRECT"
                        self.incorrect.append(file_name)
                        self.worst = top_n(self.worst, file_name, confidence, 1, False)

                    self.status[l][file_name] = status
                    self.score[l][file_name] = score
                    self.confidence[l][file_name] = confidence
                    self.avg_confidence += self.confidence[l][file_name]
                    print("file %s : %s (%s)" % (file_name, classification, score))

                    i += 1.0
                self.avg_confidence /= i

            self.accuracy = len(self.correct) / len(self.correct + self.incorrect)

def top_n(l, k, v, n, high=True):
    """
    Add v (with key k) to l such that l contains n items that are high/low value. Used to track top n/bottom n items observed
    :param l: the list
    :param n: number of items to maintain
    :param high: maintain items of high value or low value
    :return:
    """
    l.append([k, v])
    if len(l) > n:
        l = sorted(l, key=lambda entry: entry[1])
        to_remove = l[0] if high else l[-1]
        return [item for item in l if item is not to_remove]
    return l

def main():
    root_dir = sys.argv[1]
    model = Path(sys.argv[2])
    labels = Path(sys.argv[3])
    out_file = Path(sys.argv[4])

    e = Evaluator(root_dir, model, labels)

    f = open(out_file.as_posix(), 'a+')
    f.write("accuracy " + str(e.accuracy) + "\n")
    f.write("confidence " + str(e.avg_confidence) + "\n")
    for failure in e.incorrect:
        f.write("failure " + str(failure.as_posix()) + "\n")
    for best in e.best:
        f.write("best %s %s\n" % (str(best[0].as_posix()), str(best[1])))
    for worst in e.worst:
        f.write("worst %s %s\n" % (str(worst[0].as_posix()), str(worst[1])))
    f.close()


if __name__ == "__main__":
    main()
