import shutil
import xml.etree.ElementTree as ET
import os
import subprocess
from pathlib import Path
import sys
import datetime
import random
import src.ml.not_my_code.retrain as rt
import tensorflow as tf

from src.ml.preprocessor.Preprocessor import Preprocessor
from src.ml.testbench.evaluator import Evaluator


def load_configuration(config_file):
    # read cmd line args
    config_file = sys.argv[1]
    tree = ET.parse(config_file)
    root = tree.getroot()

    config = Config(root)
    tests = []

    log("CONFIGURATION:")
    for key, value in config.params.items():
        log("\t" + str(key) + " = " + str(value))
    log("")

    log("TESTS:")
    for t in root.find('tests').iter('test'):
        test = Test(t, config.params['out_dir'], config.params['base_module'])
        tests.append(test)
        log("\t"+test.name)
    log("")



    return config, tests


def init_file(xml_root, attr):
    return Path(xml_root.find(attr).text)


def init_str(xml_root, attr):
    return str(xml_root.find(attr).text)


def init_int(xml_root, attr):
    content = xml_root.find(attr).text
    return int(content) if content is not None else 0


def init_bool(xml_root, attr):
    t = xml_root.find(attr).text
    return True if t == "True" else False


class Config:
    def __init__(self, xml):
        config = xml.find('config')
        self.params = {}
        self.params['base_module'] = init_str(config, 'base_module')
        self.params['training_root'] = init_file(config, 'training_root')
        self.params['test_root'] = init_file(config, 'test_root')
        self.params['out_dir'] = init_file(config, 'out_dir')
        self.params['skip_existing_images'] = init_bool(config, 'skip_existing_images')
        self.params['keep_preprocessor_results'] = init_bool(config, 'keep_preprocessor_results')
        self.params['force_retrain'] = init_bool(config, 'force_retrain')
        self.params['num_cross_validation_folds'] = init_int(config, 'num_cross_validation_folds')

        # config for training script. image_dir is not here because it needs to be generated by each test's
        # preprocessing all values here are kept as str to allow for easy passing into script
        self.params['bottleneck_dir'] = init_str(config, 'bottleneck_dir')
        self.params['how_many_training_steps'] = init_str(config, 'how_many_training_steps')
        self.params['model_dir'] = init_str(config, 'model_dir')


class Test:
    def __init__(self, xml, out_dir, base_module=None):
        self.name = init_str(xml, 'name')
        self.modules = []
        self.directory = out_dir.joinpath(self.get_name_as_path())
        self.training_directory = self.directory.joinpath('train')
        self.test_directory = self.directory.joinpath('test')
        self.model = Path(self.directory, 'retrained_graph.pb')
        self.labels = Path(self.directory, 'retrained_labels.txt')

        if not self.directory.exists():
            os.mkdir(self.directory.as_posix())

        if not self.training_directory.exists():
            os.mkdir(self.training_directory.as_posix())

        if not self.test_directory.exists():
            os.mkdir(self.test_directory.as_posix())

        for m in xml.find('modules').text.split(','):
            self.modules.append(base_module + '.' + m if base_module is not None else m)

    def get_categories(self):
        return [d for d in self.training_directory.iterdir()]

    def get_name_as_path(self):
        return self.name.replace(' ', '_')


def log(msg):
    global output_txt

    print(msg)

    f = open(output_txt, 'a+')
    f.write('%s\n' % msg)
    f.close()


def preprocess(config, tests):
    log("Preprocessing...")
    for test in tests:
        for category in config.params['training_root'].iterdir():
            log("\t" + test.name + "(" + str(category.name) + ")")

            d = test.training_directory.joinpath(category.name)
            if not d.exists():
                os.mkdir(d.as_posix())

            # training data
            training_preprocessor = Preprocessor(Path(category),
                                                 Path(test.training_directory, category.name).as_posix(),
                                                 test.modules, config.params['skip_existing_images'])

            training_preprocessor.process()

    log("")


def train_with_config(config, tests):
    log("Training...")

    for test in tests:
        log("\t" + test.name)

        if test.model.exists() and test.labels.exists() and not config.params['force_retrain']:
            # this model has been trained already
            continue

        train(config.params['bottleneck_dir'], config.params['how_many_training_steps'], config.params['model_dir'],
              test.model.as_posix(),
              test.labels.as_posix(), test.training_directory.as_posix())
    log("")


def train(bottleneck_dir, how_many_training_steps, model_dir, output_graph, output_labels, image_dir):
    args = '--bottleneck_dir=' + bottleneck_dir
    args += ' --how_many_training_steps=' + how_many_training_steps
    args += ' --model_dir=' + model_dir
    args += ' --output_graph=' + output_graph
    args += ' --output_labels=' + output_labels
    args += ' --image_dir=' + image_dir

    # todo: make this line more general
    subprocess.call('python C:\\Users\\Ben\\PycharmProjects\\thesis\\src\\ml\\not_my_code\\retrain.py ' + args)


def run_test(test, root, model, labels, out_file) -> float:
    """
    Why the dodginess? Tensorflow has a bug whereby GPU resources aren't freed upon a session being closed. For this
    reason, we have to create a new python process to invoke tensorflow each time.
    """
    args = root + " " + model + " " + labels + " " + out_file
    subprocess.call('python evaluator.py ' + args)
    result = Path(out_file).read_text()
    accuracy = 0
    for line in result.splitlines():
        s = line.split(' ')
        key = s[0]
        value = s[1]
        if key == "accuracy":
            accuracy = value
        elif key == "failure":
            # failed file; write to faildir
            log("\t\tFailed: %s" % value)

            fail_dir = Path(test.directory, 'failures')
            if not fail_dir.exists():
                os.mkdir(fail_dir.as_posix())
            shutil.copy(value, str(fail_dir.as_posix()))
        elif key == "confidence":
            confidence = value
        elif key == "best":
            log("\t\tbest : %s -> %s" % (value, s[2]))

            best_dir = Path(test.directory, 'best')
            if not best_dir.exists():
                os.mkdir(best_dir.as_posix())
            shutil.copy(value, str(best_dir.as_posix()))
        elif key == "worst":
            log("\t\tworst : %s -> %s" % (value, s[2]))

            worst_dir = Path(test.directory, 'worst')
            if not worst_dir.exists():
                os.mkdir(worst_dir.as_posix())
            shutil.copy(value, str(worst_dir.as_posix()))

    return float(accuracy), float(confidence)


def cleanup(config):
    """
     Delete all files created by the preprocessor
    """
    # todo: implement this
    pass


def run_tests(config, tests):
    """
    Deprecated?
    """
    log("Running tests:")

    result_lines = ""

    for test in tests:
        result = Evaluator(test.test_directory, test.model, test.labels)

        total = len(result.correct + result.incorrect)
        rate = 100 * len(result.correct) / total

        r = "\t{0} -> {1}/{2} ({3}%)".format(test.name, len(result.correct), total, rate)

        result_lines += r + "\n"

    time = str(datetime.datetime.now())
    filename = "results.txt"  # todo make this different each run
    with config.params['out_dir'].joinpath(filename).open('w') as f:
        r += "Test time: {0}\n".format(time)

        # labels
        labels = ""
        for category in config.params['test_root'].iterdir():
            labels += category.name + " "
        r += "Labels: {0}\n".format(labels)

        r += "Number of Training Steps: {0}\n".format(str(config.params['how_many_training_steps']))

        r += "\nRESULTS:\n{0}".format(result_lines)

        f.writelines(r)

    if not config.params['keep_preprocessor_results']:
        cleanup(config)


def test_cross_validation(config, tests):
    log("Running tests (%d fold cross validation):" % config.params['num_cross_validation_folds'])
    # loop over tests
    results = []
    for test in tests:
        log("\tTest (" + test.name + "):")
        # collect an array of all the samples
        samples = [sample for sampledir in [list(c.iterdir()) for c in test.get_categories()] for sample in sampledir]
        random.shuffle(samples)

        partitions = []
        for i, sample in enumerate(samples):
            curr = i % config.params['num_cross_validation_folds']
            if len(partitions) <= curr:
                partitions.append([])
            partitions[curr].append(sample)

        # create directory for partions to live in while training and evaluation takes place
        tmp_dir = Path(test.directory, 'current_test_partition')
        if not tmp_dir.exists():
            os.mkdir(tmp_dir.as_posix())

        accuracy = 0.0
        confidence = 0.0
        for i, partition in enumerate(partitions):
            # prepare sample directory for training
            for sample in partition:
                curr_directory = Path(tmp_dir, sample.parent.name)
                if not curr_directory.exists():
                    os.mkdir(curr_directory.as_posix())
                shutil.move(str(sample.as_posix()), str(curr_directory.as_posix()))

            # train the model on the remaining samples in the training directory
            train(config.params['bottleneck_dir'], config.params['how_many_training_steps'], config.params['model_dir'],
                  test.model.as_posix(),
                  test.labels.as_posix(), test.training_directory.as_posix())

            # evaluate the model
            tmp = Path('partition_test_result')
            partition_test_result, partition_confidence = run_test(test, tmp_dir.as_posix(), test.model.as_posix(), test.labels.as_posix(),
                                           tmp.as_posix())
            accuracy += partition_test_result / config.params['num_cross_validation_folds']
            confidence += partition_confidence / config.params['num_cross_validation_folds']
            log("\t\tTrained model " + str(i + 1) + "/" + str(
                config.params['num_cross_validation_folds']) + " with accuracy " + str(partition_test_result))
            log("")

            os.remove(tmp.as_posix())

            # move files from partition back into their original location
            for sample in partition:
                tmp_sample = Path(Path(tmp_dir, sample.parent.name), sample.name)
                shutil.move(str(tmp_sample.as_posix()), str(sample.as_posix()))

        # remove files created during testing
        for dir in tmp_dir.iterdir():
            os.rmdir(dir.as_posix())
        os.rmdir(tmp_dir.as_posix())

        results.append((test.name, (accuracy, confidence)))

    log("")
    log("SUMMARY:")
    for (test_name, (accuracy, confidence)) in results:
        log('\t' + test_name + ': ' + str(accuracy) + ' (confidence ' + str(confidence) + ')')



def main():
    global output_txt

    # create global log file
    output_txt = datetime.datetime.now().strftime('%d%m%Y_%H-%M-%S') + '.log.txt'

    config, tests = load_configuration(sys.argv[1])
    preprocess(config, tests)

    # are we doing cross validation or regular testing?
    if config.params['num_cross_validation_folds'] > 0:
        test_cross_validation(config, tests)
    else:
        train_with_config(config, tests)
        run_tests(config, tests)


if __name__ == "__main__":
    main()
