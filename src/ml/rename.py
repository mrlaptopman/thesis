from pathlib import Path
import shutil
import sys

def main(args):
    root_dir = Path(args[1])


    for i, file in enumerate(root_dir.iterdir()):
        ext = file.as_posix().split(".")[-1]
        shutil.move(file.as_posix(), Path(root_dir, str(root_dir.name + "_" + str(i))).as_posix() + "." + ext)


if __name__ == "__main__":
    main(sys.argv)

