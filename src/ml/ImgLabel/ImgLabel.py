import cv2
import sys
import argparse
import os
from os import listdir
from os.path import isfile, join


def scaleToSize(img, sizex, sizey):
    # get dimensions of the image
    x, y = img.shape[:2]
    largest = x if x > y else y
    newMaxLength = sizex if largest is x else sizey
    factor = newMaxLength / largest
    return cv2.resize(img, None, fx=factor, fy=factor, interpolation=cv2.INTER_AREA)

def labelImg(window, img):
    while True:
        #scale up for easier viewing
        cv2.imshow(window, scaleToSize(img,1000,1000))

        # get the key pressed by the user
        key = cv2.waitKey(1) & 0xFF

        #label as "good"
        if key == ord("j"):
            decision = "label_good"
            break
        #label as "bad"
        elif key == ord("k"):
            decision = "label_bad"
            break
        #discard
        elif key == ord("l"):
            decision = "discard"
            break

    cv2.destroyWindow(window)
    return decision


#delete the file
def discard(f):
    os.remove(f)

def moveToDir(f, dest):
    os.rename(f, dest + "/" + os.path.basename(f))


def createDirIfNotExist(dir):
    if not (os.path.exists(dir) and os.path.isdir(dir)):
        os.makedirs(dir)


def main(args):
    parser = argparse.ArgumentParser(description='Label images in the provided directory.')
    parser.add_argument('rootDir', metavar='rootDir', type=str,
                        help='Directory where images are located. All images must be in the top level of this '
                             'directory.')
    parser.add_argument('outDir', metavar='outDir', type=str, help='Directory where output will be stored.')

    result = parser.parse_args()
    rootDir = result.rootDir
    outDir = result.outDir
    goodDir = outDir + "/good"
    badDir = outDir + "/bad"

    #list all files in rootDir
    inputFiles = [join(rootDir, f) for f in listdir(rootDir) if isfile(join(rootDir, f))]

    #create output directory if it does not exist
    createDirIfNotExist(outDir)
    createDirIfNotExist(goodDir)
    createDirIfNotExist(badDir)

    #loop over files found
    i = 1
    for f in inputFiles:
        currImg = cv2.imread(f, -1)
        decision = labelImg("Label me! ("+str(i)+"/"+str(len(inputFiles))+")", currImg)

        if(decision is "label_good"):
            moveToDir(f, goodDir)
        elif(decision is "label_bad"):
            moveToDir(f, badDir)
        else:
            discard(f)

        i += 1



if __name__ == "__main__":
    main(sys.argv)