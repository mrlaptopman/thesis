import cv2
import numpy as np
from itertools import product

def process(image):
    # for each pixel, substract mean intensity and divide by standard deviation

    # convert to greyscale
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # compute mean of image
    w, h = image.shape[:2]
    mean = np.sum(image, dtype=np.float64) / (w * h)
    std = np.std(image, dtype=np.float64)

    for i in range(w):
        for j in range(h):
            pixel = (image[i, j] - mean) / std
            image[i, j] = pixel

    return image
