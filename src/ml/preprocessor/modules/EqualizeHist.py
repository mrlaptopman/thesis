import cv2


def process(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY, image)
    return cv2.equalizeHist(image, image)
