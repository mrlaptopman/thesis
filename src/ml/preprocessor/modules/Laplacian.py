import cv2


def process(image):
    return cv2.Laplacian(image, cv2.CV_64F)
