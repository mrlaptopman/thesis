import sys
import shutil
import cv2
import os
from pathlib import Path


class Preprocessor():
    def __init__(self, root_dir, out_dir, modules, skip_existing=False):
        # list of modules that will be executed on the data in order
        self.modules = modules
        self.input_images = root_dir.iterdir()
        self.out_dir = out_dir
        self.buffer = None
        self.skip_existing = skip_existing

    def process(self):
        # chain modules together and save resulting image to output dir
        for image in self.input_images:
            im = Path(image)

            # skip this image if it already exists in the out_dir
            if self.skip_existing and os.path.exists(os.path.join(self.out_dir, im.name)):
                continue

            # print("Processing " + str(image))

            # initialize the file buffer
            self.buffer = Path(Path.cwd(), im.name)
            buffer = self.buffer.as_posix()
            shutil.copy(str(image), str(self.buffer))

            for current_module in self.modules:
                pivot = current_module.rfind(".")
                pkg = current_module[:pivot]
                _module = current_module[pivot + 1:]
                _temp = __import__(pkg, globals(), locals(), [_module], 0)
                instance = getattr(_temp, _module)

                image = cv2.imread(buffer)
                image = instance.process(image)
                cv2.imwrite(buffer, image)

            # copy processed file to out_dir
            shutil.copy(buffer, str(Path(self.out_dir, im.name)))

            # delete the tmp file
            self.buffer.unlink()


def main():
    # read cmd line args
    root_dir = sys.argv[1]
    out_dir = sys.argv[2]
    module_file = sys.argv[3]

    # todo add some checks to ensure both module file and root dir exist

    # create the Preprocessor with command line args
    proc = Preprocessor(Path(root_dir), out_dir, parse_modules(module_file))

    proc.process()


def parse_modules(module_file):
    modules = []
    p = Path(module_file)
    content = p.read_text()

    for line in content.split("\n"):
        modules.append(line)

    return modules


if __name__ == "__main__":
    main()
