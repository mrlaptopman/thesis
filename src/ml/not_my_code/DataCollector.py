from bs4 import BeautifulSoup
import requests
import re
import urllib2
import os
import cookielib
import json

query = raw_input("search query: ")
image_type = "ActiOn"
query = query.split()
query = '+'.join(query)
url = "https://www.google.co.in/search?q=" + query + "&source=lnms&tbm=isch"

output_directory = os.getcwd()
header = {
    'User-Agent': "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"
    }
soup = BeautifulSoup(urllib2.urlopen(urllib2.Request(url, headers=header)), 'html.parser')

large_images = []
for a in soup.find_all("div", {"class": "rg_meta"}):
    link, Type = json.loads(a.text)["ou"], json.loads(a.text)["ity"]
    large_images.append((link, Type))

print "found", len(large_images), "images"

if not os.path.exists(output_directory):
    os.mkdir(output_directory)
output_directory = os.path.join(output_directory, query.split()[0])

if not os.path.exists(output_directory):
    os.mkdir(output_directory)

for i, (img, Type) in enumerate(large_images):
    try:
        req = urllib2.Request(img, headers={'User-Agent': header})
        iamge_data = urllib2.urlopen(req).read()

        num_images = len([i for i in os.listdir(output_directory) if image_type in i]) + 1

        q = "query".replace(" ", "_")
        q = q.replace("-", "_")
        q = q.replace(".", "_")
        if len(Type) == 0:
            f = open(os.path.join(output_directory, q + "_" + str(num_images) + ".jpg"), 'wb')
        else:
            f = open(os.path.join(output_directory, q + "_" + str(num_images) + "." + Type), 'wb')

        f.write(iamge_data)
        f.close()
    except Exception as e:
        print "could not load : " + img
        print e
