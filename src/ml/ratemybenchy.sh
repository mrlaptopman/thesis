calc(){ awk "BEGIN { print "$*" }"; }
root=$1
wd=$(pwd)
export TF_CPP_MIN_LOG_LEVEL=2
num_correct=0	
total=0
for class in $(ls $root); do
	for file in $(ls $root/$class); do
		f=$wd/$root$class/$file
		r=$(python ratemybenchy.py $f | cut -d',' -f 2-3)
		classification=$(echo $r | cut -d',' -f 1)
		score=$(echo $r | cut -d',' -f 2)

		result="INCORRECT"
		if [ $classification == $class ]
		then
			result="CORRECT"
			num_correct=$((num_correct+1))
		fi
		printf "File: %s -> Classification: %s %s (%s)\n" $f $classification $score $result
		total=$((total+1))
	done
done
printf "Accuracy: %s/%s (%.2f%s)\n" $num_correct $total $(calc 100*$num_correct/$total) "%"
