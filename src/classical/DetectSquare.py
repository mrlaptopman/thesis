import colorsys
import operator

import cv2
import numpy as np

from src.classical.PrintParams import PrintParams

# point clicked by the mouse on the image

roiPoints = [()]
dragging = False
imageToCrop = None
croppedImage = None

# user_selectBedPts
pts = []

SIZEX = 500
SIZEY = 500
COLOR_TOLERANCE = 0.3  # considered same colour if its within COLOR_TOLERANCE% of the color


def user_selectROI(event, x, y, flags, param):
    global roiPoints, dragging, imageToCrop, croppedImage

    window = param["window"]
    image = param["image"]

    if event == cv2.EVENT_LBUTTONDOWN:
        roiPoints = [(x, y)]
        dragging = True
    elif event == cv2.EVENT_LBUTTONUP:
        roiPoints.append((x, y))
        dragging = False
        croppedImage = image.copy()[roiPoints[0][1]:roiPoints[1][1], roiPoints[0][0]:roiPoints[1][0]]
        cv2.imshow(window, croppedImage)
        cv2.waitKey(0)
        # elif event == cv2.EVENT_MOUSEMOVE:
        #     #draw a rectangle around the selected area
        #     #create empty matrix, draw rect on it then overlay with original
        #     rows, cols, channels = image.shape
        #     rectangle = np.zeros((rows, cols, channels))
        #     cv2.rectangle(rectangle, roiPoints[0], (x,y), (0,255,0), 2)
        #
        #     cv2.imshow(window, rectangle)


def selectROI(window, img):
    global imageToCrop, croppedImage

    rotated = img
    phi = 0
    while True:
        cv2.imshow(window, rotated)

        # get the key pressed by the user
        key = cv2.waitKey(1) & 0xFF

        # rotate clockwise if 't' is pressed, and anti-clockwise
        # if 'r' is pressed
        if key == ord("t"):
            phi += 1
            rotated = rotate(img, phi)
        elif key == ord("r"):
            phi -= 1
            rotated = rotate(img, phi)
        elif key == ord("f"):
            break  # we are done when the user presses the 'f' key

    # we select the region of interest
    imageToCrop = rotated
    cv2.setMouseCallback(window, user_selectROI, {"window": window, "image": rotated.copy()})

    # wait until the region has been selected
    cv2.waitKey(0)

    # cv2.imshow("Cropped image", croppedImage)
    return croppedImage


def rotate(img, angle):
    rows, cols = img.shape[:2]
    M = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)
    return cv2.warpAffine(img, M, (cols, rows))


# Determine how level the printbed is. The user will be
def checkBedLevel(img, printParams):
    pass


def user_selectPts(event, x, y, flags, param):
    global pts
    cv2.imshow("Target Colour", createNewImage(100, 100, param["image"][y][x]))
    if event == cv2.EVENT_LBUTTONDOWN:
        if len(pts) < param["numPts"]:
            pts.append((x, y))


def selectNPointsFromImage(img, n, windowName="Select Points"):
    global pts



    cv2.namedWindow(windowName)
    cv2.imshow(windowName, img)
    cv2.setMouseCallback(windowName, user_selectPts, {"window": windowName, "image": img, "numPts": n})

    # wait for
    while len(pts) < n:
        cv2.waitKey(1)

    bedColor = [0, 0, 0]
    for point in pts:
        # openCV stores colours in BGR
        x = point[0]
        y = point[1]
        bedColor[2] += img[y][x][2]
        bedColor[1] += img[y][x][1]
        bedColor[0] += img[y][x][0]
    bedColor = [channel / len(pts) for channel in bedColor]

    # reset our global
    pts = []

    return bedColor


# return a colour spectrum starting at bed colour and ending at filament colour
def generateSpectrum(bedColor, filamentColor):
    global COLOR_TOLERANCE
    result = {}
    height = 50
    width = 200
    res = []

    # todo: tidy up parts of code that only relate to displaying image for testing

    for i in range(width):
        bedSpec = tuple(ch * (width - i) / width for ch in bedColor)
        filSpec = tuple(ch * i / width for ch in filamentColor)  # result.append(bed[i,j] + fil[i,j]
        combinedSpec = tuple(map(operator.add,bedSpec,filSpec))
        res.append(combinedSpec)
        result[getSpectrumKey(combinedSpec)] = i / width

    # cv2.imshow("bed",bed)
    # cv2.imshow("fil",fil)

    return result


def getSpectrumKey(color):
    return str(color[0]) + "," + str(color[1]) + "," + str(color[2])

def getColorFromKey(key):
    splits = key.split(",")
    return (float(splits[0]), float(splits[1]), float(splits[2]))


def getHeat(percentage):
    #return a colour for heatmap given a percentage
    rgb = hsv2rgb(percentage, 1, 1)
    bgr = (rgb[2], rgb[1], rgb[0])
    return bgr

def hsv2rgb(h, s, v):
    return tuple(int(i * 255) for i in colorsys.hsv_to_rgb(h, s, v))

def rgb2hsv(r, g, b):
    return tuple(int(i * 255) for i in colorsys.rgb_to_hsv(r, g, b))

def showHeatMap():
    width = 400
    result = createNewImage(100,width)
    for i in range(width):
        result[:,i] = getHeat(i / width)
    return result

def thresholdSpectrum(img, spectrum):
    # specRows, specCols = spectrum.shape[:2]
    imgRows, imgCols = img.shape[:2]
    print(imgRows)

    # if pixel in image is in range of a pixel in spectrum, keep it
    result = createNewImage(imgRows, imgCols)
    heatMap = createNewImage(imgRows, imgCols)
    for r in range(imgRows):
        print(r)
        for c in range(imgCols):
            for colorStr in spectrum.keys():
                color = getColorFromKey(colorStr)
                if inRange(img[r, c], color):
                    result[r, c] = img[r, c]
                    heatMap[r,c] = getHeat(spectrum.get(colorStr))


    # threshold our image based on every pixel in the spectrum
    # imgs = imgRows * imgCols
    # result = createNewImage(imgRows, imgCols)
    # for row in range(specRows):
    #     print(row)
    #     for col in range(specCols):
    #         color = spectrum[row,col]
    #         result += thresholdRGB(img, color)



    # result[:,:] = tuple(ch / imgs for ch in result[:,:])

    # todo dont forget im dodgily resizing here
    # result = cv2.resize(result, None, fx=4, fy=4, interpolation=cv2.INTER_NEAREST)
    # heatMap = cv2.resize(heatMap, None, fx=4, fy=4, interpolation=cv2.INTER_NEAREST)

    cv2.imshow("result of thresholding spectrum", result)
    cv2.imshow("heatmap", heatMap)

    cv2.imwrite("heatmap.png", heatMap)
    cv2.imwrite("resultThreshold.png", result)
    cv2.waitKey(0)


# threshold to zero given a color
def thresholdRGB(img, color):
    # split image into b g and r channels
    b, g, r = cv2.split(img)

    max = 255
    minB = color[0]
    minG = color[1]
    minR = color[2]

    # threshold
    _, threshB = cv2.threshold(b, minB, max, cv2.THRESH_TOZERO_INV)
    _, threshG = cv2.threshold(g, minG, max, cv2.THRESH_TOZERO_INV)
    _, threshR = cv2.threshold(r, minR, max, cv2.THRESH_TOZERO_INV)

    # recombine the images
    result = cv2.merge((threshB, threshG, threshR))

    return result


def getColorBrightness(color):
    return (color[0] + color[1] + color[2]) / 3


def normalizeBrightness(img):
    # calculate avg image brightness, then brighten/darken every pixel by the % required to have that brightness
    avgBrightness = 0
    rows, cols = img.shape[:2]
    for r in range(rows):
        for c in range(cols):
            avgBrightness += getColorBrightness(img[r, c])

    avgBrightness /= rows * cols
    for r in range(rows):
        for c in range(cols):
            percentChange = (avgBrightness / getColorBrightness(img[r, c])) - 1
            img[r, c] = scaleColor(img[r, c], percentChange)

    cv2.imshow("normalized brightness", img)
    print(avgBrightness)


def histRGB(img):
    # hist each channel then recombine
    r, c = img.shape[:2]
    b, g, r = cv2.split(img)
    cv2.equalizeHist(b, b)
    cv2.equalizeHist(g, g)
    cv2.equalizeHist(r, r)
    return cv2.merge((b, g, r))


def inRange(queryColor, color):
    global COLOR_TOLERANCE
    b = color[0]
    g = color[1]
    r = color[2]
    qB = queryColor[0]
    qG = queryColor[1]
    qR = queryColor[2]
    minB = clamp(b * (1 - COLOR_TOLERANCE))
    maxB = clamp(b * (1 + COLOR_TOLERANCE))
    minG = clamp(g * (1 - COLOR_TOLERANCE))
    maxG = clamp(g * (1 + COLOR_TOLERANCE))
    minR = clamp(r * (1 - COLOR_TOLERANCE))
    maxR = clamp(r * (1 + COLOR_TOLERANCE))

    return minB <= qB <= maxB \
           and minG <= qG <= maxG \
           and minR <= qR <= maxR


def showColor(color, winName):
    cv2.imshow(winName, createNewImage(100, 100, color))


# brighten or darken color by percentage. Positive brightens, while negative darkens
def scaleColor(color, percentage):
    return tuple(clamp(ch + percentage * 255) for ch in color)


def clamp(x):
    if x < 0: return 0
    if x > 255: return 255
    return x


def createNewImage(height, width, color=(0, 0, 0)):
    img = np.zeros((height, width, 3), np.uint8)
    img[:, :] = color
    return img

def nerfBrightness(img):
    #convert to hsv and dial back the V
    pass


# Prompt the user to select some points on the print bed, and the filament. Return a PrintParams object
def getPrintParams(img):
    NUM_PTS = 10

    print("Determining bed colour: select " + str(NUM_PTS) + " by left clicking")
    bedColor = selectNPointsFromImage(img, NUM_PTS)

    print("Determining filament colour: select " + str(NUM_PTS) + " by left clicking")
    filamentColor = selectNPointsFromImage(img, NUM_PTS)

    return PrintParams(bedColor, filamentColor)


def main():
    global SIZEX, SIZEY
    windowName = "Display"
    img = cv2.imread("C:\\Users\\Ben\\Pictures\\test-square.png", -1)
    img = scaleToSize(img, SIZEX, SIZEY)
    cv2.imwrite("scaled.png", img)
    # img = selectROI(windowName, img)

    img = cv2.pyrMeanShiftFiltering(img, 40, 50)

    # img = histRGB(img)
    heat = showHeatMap()
    cv2.imwrite("heat.png", heat)
    cv2.imshow("heat",heat)
    params = getPrintParams(img)
    # thresholdRGB(img, (0,165,255))
    spec = generateSpectrum(params.bedColor, params.filamentColor)
    thresholdSpectrum(img, spec)

    # generateSpectrum((255,0,0),(0,0,255))
    # print(params)

    # img = checkBedLevel(img, params)
    # cv2.imwrite("result.png",img)
    # cv2.namedWindow(windowName)
    # cv2.imshow(windowName, img)
    cv2.waitKey(0)


# Given an image, return a resized version so that the x and y
# size of the image is at most that specified by sizex and sizey.
# Preserves aspec ratio
def scaleToSize(img, sizex, sizey):
    # get dimensions of the image
    x, y = img.shape[:2]
    largest = x if x > y else y
    newMaxLength = sizex if largest is x else sizey
    factor = newMaxLength / largest
    return cv2.resize(img, None, fx=factor, fy=factor, interpolation=cv2.INTER_AREA)


if __name__ == '__main__':
    main()
