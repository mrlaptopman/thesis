#Contains information about a printer: i.e. the colour of filament used, and the colour of the print bed
class PrintParams:

    def __init__(self, bedColor, filamentColor):
        self.filamentColor = filamentColor
        self.bedColor = bedColor