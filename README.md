# Thesis
March 29:

So far I've tried scraping for images for labelling data to no avail with Google's API; it has been deprecated.
I tried using GoogleScraper but found it to be buggy and inconsistent. The developer has seemed to have abandoned it.


April 3:

Started work on presentation.
Installed OpenCV.
Started experimenting with square detectors for first layer issues.

1 May:

Presented a couple weeks back. Got some good feedback; need to have a more general approach. Today I will begin work on 
detecting if the nozzle is too close to the build plate. Given am image of print, 
I will determine how "close" nozzle is by measuring how much the print blends into the bed.

Idea: histogram could be used to detect z wobble?

2 May:

Filament and bed color can now be selected by the user

8 May:

wrote code to generate spectrum given two colours

9 May:

using spectrum code to attempt to identify how close nozzle is: so far have tried
    * rbg thresholding for each pixel in spectrum -> does not seem to work
    * deleting every colour in image that is not in spectrum?
        * this does not work. I think because the image is 3D and my spectrum is only 2D
        * tried normalizing brightness of image to reduce color space - did not work
        * tried downsampling to reduce color space - did not work
        * will deleting the 2-3 least significant bits help?
        * what about hist? could we use that to select the colour?
    
31 July:

          Investigating the possibility of transfer learning. Will try to classify images of #3DBenchy as "good" or "bad"
          Using fatkun batch image downloader extension for chrome to download images. Wrote a quick tool in python to label
            the images as "good" or "bad" examples
          Using TensorFlow
          * Wrote tool "ImgLabel" to label images
            * takes a dir as input, allows user to quickly label images as good or bad. saves labelled images to  location
                    specified by the user
            * many batch image download programs are either cancerous, paid, or will only download thumbnails
            * downloaded and ran script from stackoverflow: https://stackoverflow.com/questions/20716842/python-download-images-from-google-image-search
                    * can only download 100 images at a time, but seems to be fine. just
                     
1 August:

        Downloaded and installed docker
        Downloaded and installed a docker container for TensorFlow
        Attempted to retrain network to recognize benchy_good dataset (contains 116 images of "good" benchy samples)
        
        sudo docker run -it -v $HOME/tf_files/benchy_good:/tf_files/benchy_good gcr.io/tensorflow/tensorflow:latest-devel
        cd /tensorflow/
        git pull
        python /tensorflow/tensorflow/examples/image_retraining/retrain.py \
            --bottleneck_dir=/tf_files/bottlenecks \
            --how_many_training_steps 500 \
            --model_dir=/tf_files/inception \
            --output_graph=/tf_files/retrained_graph.pb \
            --output_labels=/tf_files/retrained_labels.txt \
            --image_dir /tf_files/labels           
        
        the script will automatically train on images in sub-dirs (each sub-dir is its own category) procided in --image_dir
        
        by default, 10% cross validation is performed
        
3 August:

        Testing model from 1 april on a set of images I collected myself. Contains a selection of mostly bad benchy specimens,
        in varying degrees of badness
               
               Scores given to images:
                    bad1.jpg : benchybad 0.57329 (correct)
                    bad2.jpg : benchybad 0.53092 (correct)
                    bad3.jpg : benchybad 0.72555 (correct)
                    bad4.jpg : benchybad 0.68744 (correct)
                    bad5.jpg : benchybad 0.71121 (correct)
                    bad6.jpg : benchybad 0.62389 (correct)
                    bad7.jpg : benchygood 0.53694 (incorrect)
                    bad8.jpg : benchygood 0.76183 (incorrect)
                    bad9.jpg : benchygood 0.88684 (incorrect)
                    bad10.jpg : benchybad 0.88503 (correct)
                    good1.jpg : benchygood 0.60822 (correct)
                    good2.jpg : benchygood 0.9414 (correct)
                    good3.jpg : benchygood 0.93926 (correct)
                    good4.jpg : benchygood 0.87409 (correct)
                    good5.jpg : benchygood 0.56202 (correct)
                    good6.jpg : benchybad 0.76537 (incorrect)
                    
  Initial error : 4/16 (25%)
  IDEA: transform image of benchy based on pose estimate from template match to a standard pose before trainig
  Also: write script that automates passing imgaes into the model
                
7 August:

        Writing a script to automate the evaluation of model on a given test dataset. 
            For some reason, docker crashes when running python when I have loops. Will need to install native tensorflow
            and ditch docker. This approach is okay for now.
        Modifying scripts to automatically determine error rate based on directory structure
        
        root@49c1e3433eae:/tf_files# ./ratemybenchy.sh test_files/ 
        File: /tf_files/test_files/benchybad/bad1.jpg -> Classification: benchybad 0.573286 (CORRECT)
        File: /tf_files/test_files/benchybad/bad10.jpg -> Classification: benchybad 0.885029 (CORRECT)
        File: /tf_files/test_files/benchybad/bad2.jpg -> Classification: benchygood 0.530925 (INCORRECT)
        File: /tf_files/test_files/benchybad/bad3.jpg -> Classification: benchybad 0.725553 (CORRECT)
        File: /tf_files/test_files/benchybad/bad4.jpg -> Classification: benchybad 0.687743 (CORRECT)
        File: /tf_files/test_files/benchybad/bad5.jpg -> Classification: benchybad 0.711211 (CORRECT)
        File: /tf_files/test_files/benchybad/bad6.jpg -> Classification: benchybad 0.623891 (CORRECT)
        File: /tf_files/test_files/benchybad/bad7.jpg -> Classification: benchygood 0.536939 (INCORRECT)
        File: /tf_files/test_files/benchybad/bad8.jpg -> Classification: benchygood 0.761831 (INCORRECT)
        File: /tf_files/test_files/benchybad/bad9.jpg -> Classification: benchygood 0.886838 (INCORRECT)
        File: /tf_files/test_files/benchygood/good1.jpg -> Classification: benchygood 0.608223 (CORRECT)
        File: /tf_files/test_files/benchygood/good2.jpg -> Classification: benchygood 0.941422 (CORRECT)
        File: /tf_files/test_files/benchygood/good3.jpg -> Classification: benchygood 0.939262 (CORRECT)
        File: /tf_files/test_files/benchygood/good4.jpg -> Classification: benchygood 0.874094 (CORRECT)
        File: /tf_files/test_files/benchygood/good5.jpg -> Classification: benchygood 0.562019 (CORRECT)
        File: /tf_files/test_files/benchygood/good6.jpg -> Classification: benchybad 0.765369 (INCORRECT)
        Accuracy: 11/16 (68.75%)
        
8 August:

       Migrating project to Windows
       Successfully migrated to Widows. Retrained model on GPU, performs better for some reason. I have no idea why.
      
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad1.jpg, benchybad, 0.706743 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad10.jpg, benchybad, 0.903479 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad2.jpg, benchybad, 0.568391 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad3.jpg, benchybad, 0.819638 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad4.jpg, benchybad, 0.694385 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad5.jpg, benchybad, 0.625127 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad6.jpg, benchybad, 0.73257 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad7.jpg, benchybad, 0.531677 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad8.jpg, benchygood, 0.723781 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad9.jpg, benchygood, 0.819652 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchygood\good1.jpg, benchygood, 0.50152 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchygood\good2.jpg, benchygood, 0.877666 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchygood\good3.jpg, benchygood, 0.908373 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchygood\good4.jpg, benchygood, 0.642952 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchygood\good5.jpg, benchygood, 0.716796 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchygood\good6.jpg, benchybad, 0.848002 -> INCORRECT
    Accuracy: 13/16 (81.25%)
    
  
   For some reason, the model has varying performance if it is retrained with identical parameters. No idea why.
    
    
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad1.jpg, benchybad, 0.747678 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad10.jpg, benchybad, 0.912227 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad2.jpg, benchybad, 0.622216 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad3.jpg, benchybad, 0.833784 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad4.jpg, benchybad, 0.722872 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad5.jpg, benchybad, 0.665817 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad6.jpg, benchybad, 0.775564 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad7.jpg, benchybad, 0.574859 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad8.jpg, benchygood, 0.697925 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchybad\bad9.jpg, benchygood, 0.802139 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchygood\good1.jpg, benchybad, 0.53763 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchygood\good2.jpg, benchygood, 0.861204 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchygood\good3.jpg, benchygood, 0.892224 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchygood\good4.jpg, benchygood, 0.618725 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchygood\good5.jpg, benchygood, 0.672875 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_files\benchygood\good6.jpg, benchybad, 0.881205 -> INCORRECT
    Accuracy: 12/16 (75.00%)
    
   Also upgraded ratemybenchy.py to also include the functionality of ratemybenchy.sh, as the latter script is no longer needed.

14 August:

    Today I will write a tool that will allow me to apply preprocessing to the training images, and invoke the training script
    in a modular fashion using the Strategy pattern. The idea behind focusing as much as I am on ratemybenchy is that 
    if we can apply a technique that improves the result to a simple token example, that same technique should work on a 
    more complicated architecture. -> complete
    
    Other goals outstanding:
        Investigate the effect of varying number_of_training_steps
        Investigate the possibility of using Ensemble Learning
        
15 August:

    Starting off the day by looking at the effect of training on testing on black and white images
    
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad1.jpg, benchygood, 0.53178 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad10.jpg, benchybad, 0.791202 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad2.jpg, benchybad, 0.913965 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad3.jpg, benchybad, 0.61116 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad4.jpg, benchybad, 0.747851 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad5.jpg, benchybad, 0.771515 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad6.jpg, benchygood, 0.654056 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad7.jpg, benchygood, 0.631553 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad8.jpg, benchybad, 0.882405 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad9.jpg, benchygood, 0.541159 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good1.jpg, benchygood, 0.935732 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good2.jpg, benchygood, 0.95797 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good3.jpg, benchygood, 0.989146 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good4.jpg, benchygood, 0.901787 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good5.jpg, benchygood, 0.800404 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good6.jpg, benchybad, 0.562394 -> INCORRECT
    Accuracy: 11/16 (68.75%)
        
    It seems that the samples with zwobble are now being incorrectly classified as good (bad9 and bad1). 
    I should go through the datasets and relabel them to make sure I didn't incorrectly label in the first place,
    then retrain. First, however, I will try increasing the training steps to 1000.
    
    Training accuracy (steps = 500): 70%
    Training accuracy (steps = 1000): 70%
    
    After retraining with steps = 1000:
    
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad1.jpg, benchygood, 0.62308 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad10.jpg, benchybad, 0.83848 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad2.jpg, benchybad, 0.950729 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad3.jpg, benchybad, 0.661873 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad4.jpg, benchybad, 0.783621 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad5.jpg, benchybad, 0.848987 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad6.jpg, benchygood, 0.693485 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad7.jpg, benchygood, 0.653698 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad8.jpg, benchybad, 0.932439 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad9.jpg, benchygood, 0.517801 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good1.jpg, benchygood, 0.967238 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good2.jpg, benchygood, 0.974356 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good3.jpg, benchygood, 0.995519 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good4.jpg, benchygood, 0.946188 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good5.jpg, benchygood, 0.826017 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good6.jpg, benchybad, 0.507928 -> INCORRECT
    Accuracy: 11/16 (68.75%)
    
    Notice that the predictions seem more confident. What about with steps = 2000?
    
    Training accuracy (steps = 2000): 70%
    
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad1.jpg, benchygood, 0.692253 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad10.jpg, benchybad, 0.886212 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad2.jpg, benchybad, 0.972192 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad3.jpg, benchybad, 0.730777 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad4.jpg, benchybad, 0.824132 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad5.jpg, benchybad, 0.895675 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad6.jpg, benchygood, 0.736354 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad7.jpg, benchygood, 0.67898 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad8.jpg, benchybad, 0.963551 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchybad\bad9.jpg, benchybad, 0.53204 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good1.jpg, benchygood, 0.986416 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good2.jpg, benchygood, 0.987172 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good3.jpg, benchygood, 0.998364 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good4.jpg, benchygood, 0.976748 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good5.jpg, benchygood, 0.871772 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw\benchygood\good6.jpg, benchygood, 0.576323 -> CORRECT
    Accuracy: 13/16 (81.25%)
    
    
    Confidence is increased more. 
    
    
   Still, looking at the images - even though there's no more colour the brightness still varies considerably across
   the benchy samples. I don't want the system to bias brightness or colour, so we will add another filter to our
   pipeline and retrain.
   
    Training accuracy (Steps = 2000, bw + hist): 55.9%
    
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchybad\bad1.jpg, benchybad, 0.659635 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchybad\bad10.jpg, benchybad, 0.870363 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchybad\bad2.jpg, benchybad, 0.865251 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchybad\bad3.jpg, benchybad, 0.555998 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchybad\bad4.jpg, benchybad, 0.890315 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchybad\bad5.jpg, benchygood, 0.909438 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchybad\bad6.jpg, benchybad, 0.750297 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchybad\bad7.jpg, benchybad, 0.617098 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchybad\bad8.jpg, benchybad, 0.85977 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchybad\bad9.jpg, benchygood, 0.848556 -> INCORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchygood\good1.jpg, benchygood, 0.984658 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchygood\good2.jpg, benchygood, 0.912399 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchygood\good3.jpg, benchygood, 0.988663 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchygood\good4.jpg, benchygood, 0.940852 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchygood\good5.jpg, benchygood, 0.868404 -> CORRECT
    C:\Users\Ben\PycharmProjects\thesis\datasets\test_bw-hist\benchygood\good6.jpg, benchybad, 0.737802 -> INCORRECT
    Accuracy: 13/16 (81.25%)
    
21 August:

Goal of tonight is to create a framework to automate testing with various filters that will output data like so:

+---------------+----------------+-------------------+---------------+
| Preprocessing | Training Steps | Training Accuracy | Test Accuracy |
+---------------+----------------+-------------------+---------------+
| None          | 2000           |                   |               |
| Greyscale (GS)| 2000           |                   |               |
| GS + Hist Eq  | 2000           |                   |               |
| Laplacian     | 2000           |                   |               |   
| LoG           | 2000           |                   |               |
| Sobel         | 2000           |                   |               |
| Canny         | 2000           |                   |               |
| Zero Crossing | 2000           |                   |               |
| Zero LSB      | 2000           |                   |               |
+---------------+----------------+-------------------+---------------+

(note: LoG = Laplacian of Gaussian)

This will hasten the testing process dramatically! The tool will take an xml file that specifies all of the required 
configuration. The user should be able to specify several tests in a "test-chain" that would be executed in the order
of specification. The tool first perform preprocessing, then generate a model for each test case (parameters configurable), 
then evaluate that model using ratemybenchy.py


22 August:

Implemented preprocessing functionality of the automated framework - from now called "Testbench"
Now working on implementation of batch training and test execution.
Batch training implementation complete


28 August:

Working on test execution for Testbench. (complete)

Output from running testbench is currently:

    Test time: 2017-08-28 16:59:46.379067
    Labels: benchybad benchygood 
    Number of Training Steps: 500
    
    RESULTS:
    	None -> 8/16 (50.0%)
    	Greyscale (GS) -> 1/16 (6.25%)
    	Histogram Equalization -> 2/16 (12.5%)
    	Laplacian -> 10/16 (62.5%)
    	Laplacian of Gaussian (LoG) -> 7/16 (43.75%)
    	GS + Sobel -> 6/16 (37.5%)
    	Canny -> 7/16 (43.75%)
    	
I need to modify this to include the scores of each test item as in previous manual testing

Investigating Tensorflow for object detection for auto benchy cropping. Not as easy as I thought it would be,
but definately seems possible

29 August:

https://arxiv.org/pdf/1403.6382.pdf - CNN Features off-the-shelf: an Astounding Baseline for Recognition
https://deeplearningsandbox.com/how-to-use-transfer-learning-and-fine-tuning-in-keras-and-tensorflow-to-build-an-image-recognition-94b0b02444f2 - Transfer
learning for object detection

Doing some research about object detection and general CNN techniques - came across "Data Augmentation". 
This is basically applying various transformations such as pixel color jitter, rotation, shearing, random cropping, 
horizontal flipping, stretching, lens correction to inflate your dataset. You apply these various filters to your
dataset and then use the output as a part of your dataset. Supposedly this is a technique used in "every competition winning 
ConvNet". This is done automatically by Keras (I think) https://keras.io/

Tensorflow object detection:
https://github.com/GoogleCloudPlatform/tensorflow-object-detection-example
https://github.com/tensorflow/models/tree/master/object_detection 
https://github.com/tensorflow/models/blob/master/object_detection/g3doc/using_your_own_dataset.md
 
Models to use for object detection - which is best?
https://arxiv.org/pdf/1611.10012.pdf

Best model to use for accuracy: Inception Resnet V2

Are there other ways that are "good enough" or even better for segmenting benchy? Watershed algorithm perhaps?

18 Sept

Spoken to Claude. Trying to see if I can extract the cross validation results from TensorFlow. If not, will implement myself.
Goal is to now investigate: 
    * if my 70% accuracy is actually 70%. Use cross validation to do this. 
    * Is there anything in common with the samples that are being misclassified?
    
19 Sept

Implemented k-fold cross validation and greatly improved logging capabilities. Can now comfortably let tests run overnight
and view results the next day. New features include
    * incorrectly classified samples are documented and saved
    * all config information is stored in log
    * can now configure tool to perform k-fold cross validation
    * log names correspond to the time they were recorded
Going to run some tests over the following days with various parameters to see how they change the results
 